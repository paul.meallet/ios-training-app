//
//  MainAssembler.swift
//  trainingApplication
//
//  Created by paul.meallet on 07/02/2020.
//  Copyright © 2020 paul.meallet. All rights reserved.
//

import Foundation
import Swinject
import SwinjectStoryboard

class MainAssembler {
    private let assembler = Assembler(container: SwinjectStoryboard.defaultContainer)
    var resolver: Resolver {
        return assembler.resolver
    }

    init() {
        // Register services in the container at the init of the `MainAssembler`
        //Viewmodel
        assembler.apply(assemblies: [
            FilmTableViewModelAssembly(),
            FilmDescriptionViewModelAssembly(),
            FilmTableViewControllerAssembly(),
            FilmDescriptionwControllerAssembly(),
            ConnectToSocialControllerAssembly()
        ])
    }
}
