# Ios Training App

## Context

The goal of this training plan is to develop a basic movie app, that follows state-of-the-art coding patterns..

## App Specification

The app is composed of two screen (see below) :

Movie list screen, which displays a list of popular movies. Each row of the list must display :
The title of the movie
A short description of the movie
The poster movie
And other informations that you might find useful
Movie details screen, which displays more information on the movie selected :

All of the previous information
The release date
A background picture of the movie
And other informations that you might find useful
All the information can be retrieved on the following API : https://developers.themoviedb.org/3/getting-started/introduction. You must create an account to get a private API key, you can use your worldline email.

## Constraints

The app must be compatible with iPhone 5, 7, 7+, X, iPad mini and iPad in both landscape and portrait modes
The app must be developed with Xcode
The app must be developed with Swift
You can use any resources you need (Internet)
You can use any libraries you want

## Design

Here is an example of what the app can look like:





## Basics of Git

Basic operations (commit, push, pull, merge)
https://git-scm.com/docs/gittutorial
live tutorial
SwiftLint

SwiftLint is a very useful tool to perform static quality analysis on a codebase

Setting up SwiftLint on the workstation
Adding a custom .swiftlint.yml

## Development plan

Depending on your profile when you join the company, 2 methods could be used:

"Autonomous" app development, you will develop the application on your own, with daily reviews. Instructions will be given at the end to reach a state-of-the-art architecture.
Step by steps development, starting from the basics, you will discover all major Swift/iOS topics
After each (major) step, a code review must be performed.

## Step by step

Follow the following steps. Jump to the next point only if you complete all the items.

Create a project with XCode
Review application structure (pbxproj, swift, storyboard ...)
Run the application on a simulator
Run the application on a physical device
Discuss Organisation / Git
Create the Git repository
GitFlow
Merge Request
Add quality checks to your application
Add code analysis tools
Create the detail view
Hardcoded data should be set by Swift
Static text should be translated in 2 languages
Data should contains movie images
Create the list view
Add navigation: A click in the list must redirect to the details
Retrieve and display the data from an external source (API)
Connect and retrieve data from the server (See Performing Network Calls)
Make network request reactive (See Getting Started with Rx)
Architecture the application (See Application architecture)
Add MVVM architecture
Add Dependency injection to your application (See Dependency Injection)
Allow the application to run in a mock and a connected mode
Test your application
Add Unit tests
Add instrumentation tests
[Optional] Improve your application with new features
Sharing options (Social networks, mail, etc.)
Movies search field
Daily notification

## Application architecture

MVVM Architecture
https://github.com/vincent-pradeilles/swift-tips#lightweight-data-binding-for-an-mvvm-implementation
Code Separation
UI code goes in the ViewController
Business logic goes in the ViewModel
Mock data providing goes into a Service
Basics of Functional Programing
Leveraging map(), filter() and reduce()
Coding Conventions
https://github.com/raywenderlich/swift-style-guide
Memory Handling
Understanding what a retain cyle is and how to avoid it
Understanting the differences between [weak self] and [unowned self]
Type Extension
Implementing protocols on our own types (UITableViewDataSource)
Extending third-party types (factory methods on UILabel)

## Performing Network Calls

Using Carthage to integrate Alamofire
Understanding how Carthage works
Understanding the differences between Carthage and CocoaPods
Parsing a JSON stream
Making the HTTP call with Alamofire
https://github.com/Alamofire/Alamofire/blob/master/Documentation/Usage.md
Using Decodable to parse the data
https://developer.apple.com/documentation/foundation/archives_and_serialization/encoding_and_decoding_custom_types
Dealing with asynchronous code
Understanding how a completionHandler works
Understanding the differences between main thread and background threads
Using a Result type for safer coding
https://www.swiftbysundell.com/posts/the-power-of-result-types-in-swift
Formatting raw data
Leveraging Foundation APIs (DateFormatter, MeasurementFormatter)

## Dependency Injection


Abstracting service calls behind protocols
Implementing mocked services
Setting a configuration web/mocked trough a build constant
Injecting manually the right dependency given the configuration
Using Swinject
https://www.raywenderlich.com/17-swinject-tutorial-for-ios-getting-started

## Getting Started with Rx

Understanting the basics
http://reactivex.io
https://github.com/ReactiveX/RxSwift
Using the basics operators
map
flatMap
combineLastest
zip
Putting together a technical stack
Architecturing an App with Functional Reactive Programming


## Leveraging Generics

Creating a protocol Setupable to configure a UITableViewCell
protocol Setupable {
    associatedtype Model

    func setup(with model: Model)
}
Creating a protocol DataSourced to model the data to display
protocol DataSourced {
    associatedtype Model

    var dataSource: Variable<[Model]> { get }
}
Implementing a couple of GenericTableViewController and GenericTableViewModel
class GenericTableViewModel<Model>: DataSourced {

    let dataSource: Variable<[Model]> = Variable([])

    // ...

}

class GenericTableViewController<ViewModel, Cell: UITableViewCell>: UIViewController where ViewModel: DataSourced, Cell: Setupable, ViewModel.Model == Cell.Model {

    var viewModel: ViewModel?

    // ...

}
Displaying the forecasts using a subclass of GenericTableViewController
Embeding it as a child view controller

## Tests

Mocking protocol and classes
Leveraging dependancy injection to swap in mock objects
Testing the formatting logic
Testing that methods are called the appropriate number of times

## Conference Talks

Some talks that discuss some useful insights and techniques

Everyone is an API Designer - John Sundell (NSSpain 2017) https://vimeo.com/234961067
How to Control the World - Stephen Celis (NSSpain 2018) https://vimeo.com/291588126
Testing Tips & Tricks - WWDC 2018 https://developer.apple.com/videos/play/wwdc2018/417/

## Advanced Git

Complex operations (rebase, amend)
Using GitLab & Fork-flow
Forking a project
Configuring the right remotes
Updating the fork from the upstream
Creating a merge request
