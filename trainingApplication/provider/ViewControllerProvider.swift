//
//  ViewControllerProvider.swift
//  trainingApplication
//
//  Created by paul.meallet on 07/02/2020.
//  Copyright © 2020 paul.meallet. All rights reserved.
//

import Foundation
import Swinject
import SwinjectStoryboard

struct ViewControllerProvider {
}

extension ViewControllerProvider {
    struct Movies {
        static var moviesController : MovieTableViewController {
            let assembler = Assembler([FilmTableViewControllerAssembly()])
            return assembler.resolver.resolve(MovieTableViewController.self)!
        }

        static var moviesDetailController : MovieDescriptionViewController {
            let swinBoard = SwinjectStoryboard.create(name: "Main", bundle: nil)
            guard let controller = swinBoard.instantiateViewController(
                withIdentifier: "FilmDescriptionViewController") as? MovieDescriptionViewController else {
                    return MovieDescriptionViewController()}
            return controller
        }

        static var connectToSocialController : ConnectToSocialController {
           let swinBoard = SwinjectStoryboard.create(name: "Main", bundle: nil)
            guard let controller = swinBoard.instantiateViewController(
                withIdentifier: "ConnectToSocialController") as? ConnectToSocialController else {
                    return ConnectToSocialController()}
            return controller
        }
    }
}
