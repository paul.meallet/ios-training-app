//
//  AppDelegate.swift
//  trainingApplication
//
//  Created by paul.meallet on 03/02/2020.
//  Copyright © 2020 paul.meallet. All rights reserved.
//
//FIn initialisation de l'application

import UIKit
import Swinject
import SwinjectStoryboard
import FBSDKCoreKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var mainAssembler = MainAssembler()

    func application(_ application: UIApplication,didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let storyboard = SwinjectStoryboard.create(name: "Main", bundle: nil)

        guard let controller = storyboard.instantiateInitialViewController() else { return true }

        window?.rootViewController = controller

        //print("SDK version \(Settings .sdkVersion())")

        // Request Permission
        UNUserNotificationCenter.current().requestAuthorization(options: [.sound, .alert, .badge]) { granted, _ in
            if granted { print("Approval granted to send notifications") }
        }

        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)

        UNUserNotificationCenter.current().delegate = self

        return true
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        AppEvents.activateApp()
    }

    func application(_ application: UIApplication, open openUrl: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return ApplicationDelegate.shared.application(application,open: openUrl,sourceApplication: sourceApplication,annotation: annotation)
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void
    ) {
        completionHandler(.alert)
    }
}
