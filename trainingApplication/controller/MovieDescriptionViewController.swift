//
//  MovieDescriptionViewController.swift
//  trainingApplication
//
//  Created by paul.meallet on 04/02/2020.
//  Copyright © 2020 paul.meallet. All rights reserved.
//

import UIKit
import Swinject
import SwinjectStoryboard
import Foundation
import Social
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKPlacesKit
import FBSDKLoginKit
import NotificationCenter

final class MovieDescriptionViewController: UIViewController, SDWebImageProtocol {

    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var movieCoverImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var movieDescriptionLabel: UILabel!
    @IBOutlet var shareButton: UIButton!

    var movieDetailViewModel : MovieDescriptionViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        guard let currentFilm = movieDetailViewModel?.selectedFilm else { return }

        NotificationManager.sharedInstance.createNotification(with: currentFilm)
        updateUI()
    }

    @IBAction func shareButtonClicked(_ sender: Any) {
        let alert = UIAlertController(title: "Share movie",
                                      message: "On which social network whould you share ?", preferredStyle: .actionSheet)
        let facebookAction = UIAlertAction(title: "FACEBOOK", style: .default) { _ in

            let content = ShareLinkContent()

            guard let contentURL = NSURL(string: "https://developers.facebook.com") as URL? else { return }

            content.contentURL = contentURL

            let dialog = ShareDialog()
            dialog.fromViewController = self
            dialog.shareContent = content
            dialog.show()
        }

        let twitterAction = UIAlertAction(title: "TWITTER", style: .default) { _ in
            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter) {
                let tweetShare = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
                if let tweetShare = tweetShare {
                    tweetShare.setInitialText("Excellent development")
                    tweetShare.add(URL(string: "https://stackoverflow.com/users/4600136/mr-javed-multani?tab=profile"))
                    self.present(tweetShare, animated: true, completion: nil)
                }
            } else {
                self.showAlert("Twitter")
            }
        }

        let otherAction = UIAlertAction(title: "Autre", style: .default) { _ in
            let post = UIActivityViewController(
                activityItems: ["SALUT","URL to share"],
                applicationActivities: nil
            )
            post.popoverPresentationController?.sourceView = self.view
            self.present(post, animated: true,completion: nil)
        }

        alert.addAction(twitterAction)
        alert.addAction(facebookAction)
        alert.addAction(otherAction)

        present(alert,animated: true,completion: nil)
    }

    private func showAlert(_ serviceName : String) {
        let alert = UIAlertController(title: "Error",
                                      message: "You are not connected to" + serviceName + ". Connect and try again.",
                                      preferredStyle: .alert)
        let action = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
        alert.addAction(action)
        present(alert,animated: true,completion: nil)
    }

    private func updateUI() {
        guard let currentFilm = movieDetailViewModel?.selectedFilm else { return }

        movieTitleLabel.text = currentFilm.title
        dateLabel.text = currentFilm.date
        movieDescriptionLabel.text = currentFilm.movieDescription
        movieDescriptionLabel.sizeToFit()
        setMovieImage()
        setMovieBackroungImage()
    }
    private func setMovieImage() {
        guard let requestURL = movieDetailViewModel?.getImageURL(imageType: Movie.ImageType.movieCover) else { return }

        setImageViewFromUrl(movieCoverImageView,requestURL)
    }

    private func setMovieBackroungImage() {
        guard let requestURL = movieDetailViewModel?.getImageURL(imageType: Movie.ImageType.backgroundImage) else { return }

        setImageViewFromUrl(backgroundImageView,requestURL)
    }
}

class FilmDescriptionwControllerAssembly : Assembly {
    func assemble(container: Container) {
        container.storyboardInitCompleted(MovieDescriptionViewController.self, initCompleted: { resolver, controller in
            controller.movieDetailViewModel = resolver.resolve(MovieDescriptionViewModel.self)
        })
    }
}

extension MovieDescriptionViewController: SharingDelegate {
    func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {
        print(results)
    }

    func sharer(_ sharer: Sharing, didFailWithError error: Error) {
        print(error.localizedDescription)
    }

    func sharerDidCancel(_ sharer: Sharing) {
        print("Cancel sharing")
    }
}
