//
//  ConnectToSocialController.swift
//  trainingApplication
//
//  Created by paul.meallet on 17/02/2020.
//  Copyright © 2020 paul.meallet. All rights reserved.
//

import Foundation
import UIKit
import Swinject

final class ConnectToSocialController : UIViewController {}

class ConnectToSocialControllerAssembly : Assembly {
    func assemble(container: Container) {
         container.storyboardInitCompleted(ConnectToSocialController.self, initCompleted: { _, _ in
            //controller.movieDetailViewModel = resolver.resolve(FilmDescriptionViewModel.self)
         })
    }
}
