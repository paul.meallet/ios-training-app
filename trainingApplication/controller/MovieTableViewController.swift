//
//  MovieTableViewController.swift
//  trainingApplication
//
//  Created by paul.meallet on 03/02/2020.
//  Copyright © 2020 paul.meallet. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SDWebImage
import Swinject

final class MovieTableViewController: GenericTableViewController<MoviesTableViewModel, FilmTableViewCell> {
    @IBOutlet var movieSearchBar: UISearchBar!
    var searching = false
    var disposeBag : DisposeBag?
    func setup (disposeBag: DisposeBag) {
        self.disposeBag = disposeBag
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        movieSearchBar.delegate = self
        movieSearchBar.scopeButtonTitles = ["Date","A-Z","Z-A"]
        bindMovies()
        viewModel?.getFilms()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rowss
        guard let nbValue = viewModel?.dataSource.value.count else {return 0}
        return nbValue
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "FilmTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
                        as? FilmTableViewCell else {
            fatalError("Problem dequeued FilmTableViewCell.")
        }
        guard let currentFilm = viewModel?.dataSource.value[indexPath.row] else {return cell}
        //setcell in class
        guard let imgUrl = viewModel?.getImageURL(movie: currentFilm) else {return cell}
        cell.imageUrl = imgUrl
        cell.setup(with: currentFilm)

        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel?.setLastFilmClicked(index: indexPath.row)
        let controller = ViewControllerProvider.Movies.moviesDetailController
        controller.movieDetailViewModel?.selectedFilm = self.viewModel?.lastFilmClicked
        navigationController?.pushViewController(controller, animated: true)
    }
    private func bindMovies() {
        guard let disposeBag = disposeBag else {return}
        self.viewModel?.dataSource
            .asDriver()
            .drive(onNext: { _ in
                self.tableView.reloadData()
            }).disposed(by: disposeBag)
    }
}

// MARK: - UISearchBarDelegate
extension MovieTableViewController : UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //operation on movie list
        self.viewModel?.filterDatasource(searchText: searchText)
        self.viewModel?.isSearching = true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.viewModel?.isSearching = false
        self.viewModel?.setDatasourceInitialValue()
        searchBar.text = ""
    }
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        switch selectedScope {
        case 0:
            viewModel?.setDataSourceSorted(with: SortListType.sortByDate)
        case 1:
            viewModel?.setDataSourceSorted(with: SortListType.sortAtoZ)
        case 2:
            viewModel?.setDataSourceSorted(with: SortListType.sortZtoA)
        default:
            print("Error in sorted type")
        }
        tableView.reloadData()
    }
}
class FilmTableViewControllerAssembly : Assembly {
    func assemble(container: Container) {
        container.storyboardInitCompleted(MovieTableViewController.self,
                                          initCompleted: { (resolver, controller) in
            controller.disposeBag = DisposeBag()
            controller.viewModel = resolver.resolve(MoviesTableViewModel.self)
        })
    }
}
