//
//  FilmResponseBody.swift
//  trainingApplication
//
//  Created by paul.meallet on 05/02/2020.
//  Copyright © 2020 paul.meallet. All rights reserved.
//

import UIKit

struct FilmResponseBody : Decodable {
    enum  CodingKeys: String, CodingKey {
        case movies = "results"
    }
    var movies : [Movie]
}
