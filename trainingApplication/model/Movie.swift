//
//  Film.swift
//  trainingApplication
//
//  Created by paul.meallet on 03/02/2020.
//  Copyright © 2020 paul.meallet. All rights reserved.
//

import UIKit

class Movie: NSObject, Codable {
    internal init(title: String?, photoPath: String?, backdropPath: String?,
                  movieDescription: String?, date: String?) {
        self.title = title
        self.photoPath = photoPath
        self.movieDescription = movieDescription
        self.date = date
    }
    var title : String?
    var photoPath : String?
    var backdropPath : String?
    var movieDescription : String?
    var date : String?
    var photoImage: UIImage?
    enum CodingKeys: String, CodingKey {
        case title = "title"
        case photoPath =  "poster_path"
        case backdropPath = "backdrop_path"
        case movieDescription = "overview"
        case date = "release_date"
    }
    enum ImageType {
        case backgroundImage
        case movieCover
    }

}
