//
//  FilmTableViewCell.swift
//  trainingApplication
//
//  Created by paul.meallet on 03/02/2020.
//  Copyright © 2020 paul.meallet. All rights reserved.
//

import UIKit

class FilmTableViewCell: UITableViewCell, Setupable, SDWebImageProtocol {
    typealias Model = Movie
    //properties
    @IBOutlet var photoImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    var imageUrl = NSURL(fileURLWithPath : "")
    func setup(with model: Model) {
        titleLabel.text = model.title
        descriptionLabel.text = model.movieDescription
        setImageViewFromUrl(photoImageView, imageUrl)
    }
}
