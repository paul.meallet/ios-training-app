//
//  SDWebImageProtocol.swift
//  trainingApplication
//
//  Created by paul.meallet on 11/02/2020.
//  Copyright © 2020 paul.meallet. All rights reserved.
//

import Foundation
import UIKit

protocol SDWebImageProtocol {
}

extension SDWebImageProtocol {
     func setImageViewFromUrl(_ image : UIImageView,_ imageUrl: NSURL) {
        image.sd_setImage(with: imageUrl as URL?,
                          completed: {(_,downloadException, _, downloadedUrl) in
            if let downloadException = downloadException {
                print("Error dowloading image : \(downloadException.localizedDescription)")
            } else {
                print("Success dowloading image : \(downloadedUrl?.absoluteString ?? "")")
            }
        })
    }
}
