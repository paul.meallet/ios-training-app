//
//  GenericTableViewModel.swift
//  trainingApplication
//
//  Created by paul.meallet on 11/02/2020.
//  Copyright © 2020 paul.meallet. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class GenericTableViewModel<Model>: DataSourced {
    var dataSource = BehaviorRelay<[Model]>(value : [])
}
