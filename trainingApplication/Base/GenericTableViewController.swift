//
//  GenericTableViewController.swift
//  trainingApplication
//
//  Created by paul.meallet on 11/02/2020.
//  Copyright © 2020 paul.meallet. All rights reserved.
//

import Foundation
import UIKit

class GenericTableViewController<ViewModel, Cell: UITableViewCell>: UITableViewController
    where ViewModel: DataSourced, Cell: Setupable, ViewModel.Model == Cell.Model {
    var viewModel: ViewModel?
}
