//
//  Setupable.swift
//  trainingApplication
//
//  Created by paul.meallet on 11/02/2020.
//  Copyright © 2020 paul.meallet. All rights reserved.
//

import Foundation

// Quand tu vas conformer une classe à ce protocol, tu devras aussi affecter à ta classe un type associé
// Tu devras aussi implémenter la méthode `setup` avec le model que tu auras définis.
//
// Cela permet d'initialiser tout tes modèles de la même manière et d'unifier le code !
// Nous allons notamment l'utiliser pour setup tes cellules de la tableView

protocol Setupable {
    associatedtype Model

    func setup(with model: Model)
}
