//
//  NotificationManager.swift
//  trainingApplication
//
//  Created by paul.meallet on 17/02/2020.
//  Copyright © 2020 paul.meallet. All rights reserved.
//

import UIKit
import UserNotifications

final class NotificationManager: NSObject {
    static let sharedInstance = NotificationManager()

    func createNotification(with movie: Movie) {
        let notificationContent = UNMutableNotificationContent()
        notificationContent.title = "BUY IT NOW !"
        notificationContent.subtitle = "The movie " + (movie.title ?? "")
        notificationContent.body = movie.movieDescription ?? ""
        NotificationManager.sharedInstance.scheduleNotification(notifContent: notificationContent, inSeconds: 5, completion: { success in
            if success {
                print("Successfully scheduled notification")
            } else {
                print("Error scheduling notification")
            }
        })
    }

    func scheduleNotification(notifContent : UNMutableNotificationContent, inSeconds: TimeInterval, completion: @escaping (Bool) -> Void) {
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: inSeconds, repeats: false)
        let request = UNNotificationRequest(identifier: "TEst", content: notifContent, trigger: trigger)

        UNUserNotificationCenter.current().add(request, withCompletionHandler: { error in
            if error != nil {
                print("\(String(describing: error))")
                completion(false)
            } else {
                completion(true)
            }
        })
    }
}
