//
//  SortListTypeEnum.swift
//  trainingApplication
//
//  Created by paul.meallet on 12/02/2020.
//  Copyright © 2020 paul.meallet. All rights reserved.
//

import Foundation

enum SortListType {
    case sortByDate
    case sortAtoZ
    case sortZtoA
}
