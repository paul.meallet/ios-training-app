//
//  SharingSocialMediaManager.swift
//  trainingApplication
//
//  Created by paul.meallet on 14/02/2020.
//  Copyright © 2020 paul.meallet. All rights reserved.
//

import Foundation
import Social
import FBSDKCoreKit
import FBSDKShareKit

protocol shareProtocol {
    func publishToFacebook(controller : UIViewController, sharingDelegate : SharingDelegate, movietoShare: Movie)
    func publishToTwitter()
}

class ShareSocialManager : shareProtocol {
    func publishToTwitter() {
        //TODOs
    }

    func publishToFacebook(controller: UIViewController, sharingDelegate: SharingDelegate, movietoShare: Movie) {
        let shareContent = ShareLinkContent()
        guard let contentURL = URL.init(string: "https://developers.facebook.com") else {return}

        let post = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
        post?.setInitialText("Bonjour test")
        shareContent.contentURL = contentURL
        shareContent.quote = "Text to share"
        ShareDialog(fromViewController: controller, content: shareContent, delegate: sharingDelegate).show()

    }
}
