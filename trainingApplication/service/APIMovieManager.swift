//
//  APIFilmManager.swift
//  trainingApplication
//
//  Created by paul.meallet on 04/02/2020.
//  Copyright © 2020 paul.meallet. All rights reserved.
//

import UIKit
import Alamofire
import RxCocoa
import RxSwift

protocol ApiFilmProtocol {
    func fetchFamousFilm()
}

class APIMovieManager: ApiFilmProtocol {
    let APYKEY = "3d52fead94fa2965cf2fc4a6eeff6ec7"
    struct ApyKey: Encodable {
        let apyKey : String
    }
    let moviesChangedPropertyRelay = BehaviorRelay<[Movie]?>(value: nil)
    public func fetchFamousFilm() {
        var parsedData = [Movie]()
        AF.request("http://api.themoviedb.org/3/discover/movie?api_key=3d52fead94fa2965cf2fc4a6eeff6ec7")
            .responseJSON { response in
                    print(response.result)   // result of response serialization
            switch response.result {
            case .success:
                print("Validation Successful")
                let json = response.data
                do {
                    let decoder = JSONDecoder()
                    parsedData = try decoder.decode( FilmResponseBody.self, from: json!).movies
                    //get all images
                    print(parsedData)
                    self.moviesChangedPropertyRelay.accept(parsedData)
                } catch let error {
                    print(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
}
