//
//  MoviesTableViewModel.swift
//  trainingApplication
//
//  Created by paul.meallet on 04/02/2020.
//  Copyright © 2020 paul.meallet. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwinjectStoryboard
import Swinject

final class MoviesTableViewModel : GenericTableViewModel<Movie> {
    private let apiFIlm : APIMovieManager
    private let disposeBag : DisposeBag
    var isSearching = false
    var lastFilmClicked : Movie?
    var copyOriginalDatasource : [Movie]
    let baseUrl = "http://image.tmdb.org/t/p/w185/"

    init(apiManager: APIMovieManager, disposeb: DisposeBag) {
        self.apiFIlm = apiManager
        self.disposeBag = disposeb
        self.copyOriginalDatasource = [Movie]()
        //keep original datasource
        super.init()
        bindFilm()
    }
    public func getCurrentFilms() -> [Movie] {
        return dataSource.value
    }
    public func setDataSourceSorted(with sortedType : SortListType) {
        switch sortedType {
        case .sortByDate:
            self.dataSource.accept(dataSource.value.sorted(by: { $0.date ?? "" > $1.date ?? ""}))
        case .sortAtoZ:
            self.dataSource.accept(dataSource.value.sorted(
                by: { $0.title?.lowercased() ?? "" < $1.title?.lowercased() ?? ""}))
        case .sortZtoA:
            self.dataSource.accept(dataSource.value.sorted(
                by: { $0.title?.lowercased() ?? "" > $1.title?.lowercased() ?? ""}))
        }
    }
    public func setDatasourceInitialValue() {
        dataSource.accept(copyOriginalDatasource)
    }
    public func filterDatasource(searchText : String) {
        let tmpDatasource = copyOriginalDatasource
        if searchText != "" {
            self.dataSource.accept(
                tmpDatasource.filter {($0.title?.lowercased().contains(searchText.lowercased()) ?? false) }
            )
         } else {
            setDatasourceInitialValue()
         }
    }
    public func getFilms() {
        apiFIlm.fetchFamousFilm()
        //update FILM and notify the controller
    }
    private func bindFilm() {
        apiFIlm.moviesChangedPropertyRelay
            .asDriver()
            .drive(onNext: { movies in
                //publish to observer controller
                guard let movies = movies else {return}
                self.dataSource.accept(movies)
                self.copyOriginalDatasource = self.dataSource.value
            }).disposed(by: disposeBag)
    }
    public func getImageURL (movie : Movie) -> NSURL {
         let completeRequestString = self.baseUrl + (movie.photoPath)!
         return  NSURL(string : completeRequestString)!
     }
    public func setLastFilmClicked(index: Int) {
        self.lastFilmClicked = dataSource.value[index]
    }
}

class FilmTableViewModelAssembly : Assembly {
    func assemble(container: Container) {
        container.register(MoviesTableViewModel.self) { _ in
            let apiManager = APIMovieManager()
            let disposeBag = DisposeBag()
            return MoviesTableViewModel(apiManager: apiManager, disposeb: disposeBag)
        }
    }
}
