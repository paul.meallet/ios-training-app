//
//  MovieDescriptionViewModel.swift
//  trainingApplication
//
//  Created by paul.meallet on 05/02/2020.
//  Copyright © 2020 paul.meallet. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwinjectStoryboard
import Swinject
import FBSDKCoreKit
import FBSDKShareKit

final class MovieDescriptionViewModel: NSObject {
    private let apiMovie : APIMovieManager
    private let shareManager : ShareSocialManager
    private let disposeBag : DisposeBag
    let baseUrl = "http://image.tmdb.org/t/p/w185/"
    public var selectedFilm : Movie?

    init(apiManager: APIMovieManager, disposeb: DisposeBag, shareManager: ShareSocialManager) {
        self.shareManager = shareManager
        self.apiMovie = apiManager
        self.disposeBag = disposeb
        super.init()
    }

    func getImageURL (imageType : Movie.ImageType) -> NSURL {
        let completeRequestString : String
        if (imageType == Movie.ImageType.backgroundImage) {
            completeRequestString = self.baseUrl + (self.selectedFilm?.backdropPath!)!
        } else {
            completeRequestString = self.baseUrl + (self.selectedFilm?.photoPath!)!
        }
        return  NSURL(string : completeRequestString)!
    }

    func publishToFacebook(pcontroller: UIViewController, psharingDelegate: SharingDelegate) {
        guard let currentMovie = selectedFilm else { return }

        shareManager.publishToFacebook(controller: pcontroller, sharingDelegate: psharingDelegate, movietoShare: currentMovie)
    }

}

class FilmDescriptionViewModelAssembly : Assembly {
    func assemble(container: Container) {
        container.register(MovieDescriptionViewModel.self) { _ in
            let apiManager = APIMovieManager()
            let shareSocialManager = ShareSocialManager()
            let disposeBag = DisposeBag()
            return MovieDescriptionViewModel(apiManager: apiManager, disposeb: disposeBag, shareManager: shareSocialManager)
        }
    }
}
